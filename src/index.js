import React from "react";
import ReactDOM from "react-dom";
import App from "./Practice";
import "./App.css";
import Paint from "./components/Paint";

ReactDOM.render(
  <React.StrictMode>
    <Paint />
  </React.StrictMode>,
  document.getElementById("root")
);
