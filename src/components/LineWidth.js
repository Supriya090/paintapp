import React from "react";

export default function LineWidth({ lineWidth, setLineWidth }) {
  return (
    <div className='line-width'>
      <label>Line Width </label>
      <input
        type='range'
        onChange={(e) => setLineWidth(e.target.value)}
        value={lineWidth}
        min='1'
        max='10'
      />
      {lineWidth}
    </div>
  );
}
