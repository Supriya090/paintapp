import React, { useState, useEffect, useRef, useCallback } from "react";
import ColorPicker from "./ColorPicker";
import Name from "./Name";
import randomColor from "randomcolor";
import useWindowSize from "./WindowSize";
import Canvas from "./Canvas";
import RefreshButton from "./RefreshButton";
import LineWidth from "./LineWidth";

function Paint() {
  const [colors, setColors] = useState([]);
  const [activeColor, setActiveColor] = useState(null);
  const [lineWidth, setLineWidth] = useState(5);

  // const headerRef = useRef({ offsetHeight: 0 });

  const getColors = () => {
    const baseColor = randomColor().slice(1);
    fetch(`https://www.thecolorapi.com/scheme?hex=${baseColor}&mode=monochrome`)
      .then((res) => res.json())
      .then((res) => {
        setColors(res.colors.map((color) => color.hex.value));
        setActiveColor(res.colors[0].hex.value);
      });
  };

  useEffect(() => {
    getColors();
  }, []);

  let timeoutId = useRef();
  const [visible, setVisible] = useState(false);
  const [windowWidth, windowHeight] = useWindowSize(() => {
    setVisible(true);
    clearTimeout(timeoutId.current);
    timeoutId = setTimeout(() => setVisible(false), 500);
  });

  const cb = useCallback(() => getColors(), []);

  return (
    <div>
      <header style={{ borderTop: `10px solid ${activeColor}` }}>
        <div className='app'>
          <Name />
        </div>
        <div style={{ marginTop: 10 }}>
          <ColorPicker
            colors={colors}
            activeColor={activeColor}
            setActiveColor={setActiveColor}
          />
          <button
            onClick={() => {
              setActiveColor("white");
              setLineWidth(10);
            }}>
            Eraser
          </button>
          <RefreshButton cb={cb} />
        </div>
        <hr style={{ width: "100%" }} />
        <LineWidth lineWidth={lineWidth} setLineWidth={setLineWidth} />
      </header>

      {activeColor && (
        <Canvas
          color={activeColor}
          height={window.innerHeight}
          lineWidth={lineWidth}
        />
      )}
      <div className={`window-size ${visible ? "" : "hidden"}`}>
        {windowWidth} x {windowHeight}
      </div>
    </div>
  );
}

export default Paint;
